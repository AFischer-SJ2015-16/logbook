package at.spengergasse.logbook.domain;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
public class Employee extends BaseDomain {
    @Column(name = "Firstname")
    @Length(max = 50)
    @NotNull
    @NonNull
    private String firstname;
    @Column(name = "Lastname")
    @Length(max = 50)
    @NotNull
    @NonNull
    private String lastname;
    @Column(name = "SVNR")
    @Length(max = 10)
    @NotNull
    @NonNull
    private String svnr;
    @OneToMany(mappedBy = "employee")
    private List<Reservation> reservations;
    @OneToMany(mappedBy = "employee")
    private List<Trip> trips;

    @Builder
    public Employee(String id, String fn, String ln, String sv) {
        super(id);
        firstname = fn;
        lastname = ln;
        svnr = sv;
    }
}
