package at.spengergasse.logbook.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
public class Trip extends BaseDomain {
    @JoinColumn(name = "VehicleID", columnDefinition = "character")
    @ManyToOne
    @NotNull
    @NonNull
    private Vehicle vehicle;
    @JoinColumn(name = "EmployeeID", columnDefinition = "character")
    @ManyToOne
    @NotNull
    @NonNull
    private Employee employee;
    @Column(name = "startdate")
    @NotNull
    @NonNull
    private LocalDateTime startDate;
    @Column(name = "enddate")
    private LocalDateTime endDate;
    @Column(name = "kmbegin", columnDefinition = "decimal")
    @Digits(integer = 7, fraction = 2)
    @NotNull
    @NonNull
    private BigDecimal kmBegin;
    @Column(name = "kmend", columnDefinition = "decimal")
    @Digits(integer = 7, fraction = 2)
    private BigDecimal kmEnd;

    @Builder
    public Trip(String id, Vehicle veh, Employee emp, LocalDateTime sd, LocalDateTime ed, BigDecimal kb, BigDecimal ke) {
        super(id);
        vehicle = veh;
        employee = emp;
        startDate = sd;
        endDate = ed;
        kmBegin = kb;
        kmEnd = ke;
    }
}
