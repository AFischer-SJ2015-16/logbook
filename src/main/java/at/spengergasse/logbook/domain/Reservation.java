package at.spengergasse.logbook.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
public class Reservation extends BaseDomain {
    @JoinColumn(name = "VehicleID", columnDefinition = "character")
    @ManyToOne
    @NotNull
    @NonNull
    private Vehicle vehicle;
    @JoinColumn(name = "EmployeeID", columnDefinition = "character")
    @ManyToOne
    @NotNull
    @NonNull
    private Employee employee;
    @Column(name = "datefrom")
    @NotNull
    @NonNull
    private LocalDateTime dateFrom;
    @Column(name = "dateto")
    @NotNull
    @NonNull
    private LocalDateTime dateTo;

    @Builder
    public Reservation(String id, Vehicle veh, Employee emp, LocalDateTime df, LocalDateTime dt) {
        super(id);
        vehicle = veh;
        employee = emp;
        dateFrom = df;
        dateTo = dt;
    }
}
