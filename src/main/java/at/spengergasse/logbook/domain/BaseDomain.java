package at.spengergasse.logbook.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@MappedSuperclass
public abstract class BaseDomain {
    @Column(name = "ID", columnDefinition = "character")
    @NonNull
    @Id
    private String id;
}
