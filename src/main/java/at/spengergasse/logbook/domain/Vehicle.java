package at.spengergasse.logbook.domain;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
@Entity
public class Vehicle extends BaseDomain {
    @Column(name = "Type")
    @Length(max = 25)
    @NotNull
    @NonNull
    private String type;
    @Column(name = "Brand")
    @Length(max = 32)
    @NotNull
    @NonNull
    private String brand;
    @Column(name = "Model")
    @Length(max = 25)
    @NotNull
    @NonNull
    private String model;
    @Column(name = "numberplate")
    @Length(max = 50)
    @NotNull
    @NonNull
    private String numberPlate;
    @Column(name = "seatnr")
    @NotNull
    private int seatNr;
    @Column(name = "Km", columnDefinition = "decimal")
    @Digits(integer = 7, fraction = 2)
    @NotNull
    @NonNull
    private BigDecimal km;
    @Column(name = "basicprice", columnDefinition = "decimal")
    @Digits(integer = 4, fraction = 4)
    @NotNull
    @NonNull
    private BigDecimal basicPrice;
    @Column(name = "includedkm")
    private int includedKm;
    @Column(name = "priceper100km", columnDefinition = "decimal")
    @Digits(integer = 4, fraction = 4)
    private BigDecimal pricePer100km;
    @Column(name = "penaltyperday", columnDefinition = "decimal")
    @Digits(integer = 4, fraction = 4)
    private BigDecimal penaltyPerDay;
    @OneToMany(mappedBy = "vehicle")
    private List<Reservation> reservations;
    @OneToMany(mappedBy = "vehicle")
    private List<Trip> trips;

    @Builder
    public Vehicle(String id, String t, String b, String m, String np, int s, BigDecimal k, BigDecimal bp, int ik, BigDecimal ppkm, BigDecimal ppd) {
        super(id);
        type = t;
        brand = b;
        model = m;
        numberPlate = np;
        seatNr = s;
        km = k;
        basicPrice = bp;
        includedKm = ik;
        pricePer100km = ppkm;
        penaltyPerDay = ppd;
    }
}
