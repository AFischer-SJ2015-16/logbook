package at.spengergasse.logbook.config;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class JpaConfig {
    private JPAQueryFactory jpaQueryFactory;

    @Autowired
    public JpaConfig(EntityManager entityManager) {
        jpaQueryFactory = new JPAQueryFactory(entityManager);
    }

    @Bean
    public JPAQueryFactory jpaQueryFactory() {
        return jpaQueryFactory;
    }
}
