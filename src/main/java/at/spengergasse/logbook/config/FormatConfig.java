package at.spengergasse.logbook.config;

public class FormatConfig {
    public static final String DATE_REGEX = "[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}";
    public static final String GUID_REGEX = "[A-Za-z0-9]{8,8}-[A-Za-z0-9]{4,4}-[A-Za-z0-9]{4,4}-[A-Za-z0-9]{4,4}-[A-Za-z0-9]{12,12}";
}
