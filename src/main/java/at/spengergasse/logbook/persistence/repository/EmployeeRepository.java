package at.spengergasse.logbook.persistence.repository;

import at.spengergasse.logbook.domain.Employee;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends BaseRepository<Employee> {
}
