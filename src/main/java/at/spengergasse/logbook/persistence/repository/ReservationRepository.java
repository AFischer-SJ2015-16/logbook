package at.spengergasse.logbook.persistence.repository;

import at.spengergasse.logbook.domain.Reservation;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends BaseRepository<Reservation> {
}
