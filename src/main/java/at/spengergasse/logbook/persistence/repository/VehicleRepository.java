package at.spengergasse.logbook.persistence.repository;

import at.spengergasse.logbook.domain.Vehicle;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepository extends BaseRepository<Vehicle> {
}
