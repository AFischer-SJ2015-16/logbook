package at.spengergasse.logbook.persistence.repository;

import at.spengergasse.logbook.domain.BaseDomain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface BaseRepository<T extends BaseDomain> extends JpaRepository<T, String>, QueryDslPredicateExecutor<T> {
}
