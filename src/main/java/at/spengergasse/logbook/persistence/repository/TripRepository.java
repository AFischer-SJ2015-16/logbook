package at.spengergasse.logbook.persistence.repository;

import at.spengergasse.logbook.domain.Trip;
import org.springframework.stereotype.Repository;

@Repository
public interface TripRepository extends BaseRepository<Trip> {
}
