package at.spengergasse.logbook.persistence.predicate;

import at.spengergasse.logbook.domain.QReservation;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ReservationPredicate {

    public static BooleanExpression inTimespan(LocalDateTime begin, LocalDateTime end) {
        return inTimespan(QReservation.reservation, begin, end);
    }

    public static BooleanExpression notInTimespan(QReservation qReservation, LocalDateTime begin, LocalDateTime end) {
        return inTimespan(qReservation, begin, end).not();
    }

    public static BooleanExpression inTimespan(QReservation qReservation, LocalDateTime begin, LocalDateTime end) {
        return qReservation.dateFrom.lt(end).and(qReservation.dateTo.goe(begin));
    }
}
