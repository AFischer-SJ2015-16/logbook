package at.spengergasse.logbook.persistence.predicate;

import at.spengergasse.logbook.domain.QEmployee;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EmployeePredicate {

    public static BooleanExpression hasId(String id) {
        return QEmployee.employee.id.eq(id);
    }
}
