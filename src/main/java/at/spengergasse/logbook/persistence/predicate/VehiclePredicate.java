package at.spengergasse.logbook.persistence.predicate;

import at.spengergasse.logbook.domain.QVehicle;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class VehiclePredicate {

    public static BooleanExpression hasSeatNr(int seatNr) {
        return QVehicle.vehicle.seatNr.eq(seatNr);
    }

    public static BooleanExpression isBrand(String brand) {
        return QVehicle.vehicle.brand.eq(brand);
    }

    public static BooleanExpression isModel(String model) {
        return QVehicle.vehicle.model.eq(model);
    }

    public static BooleanExpression hasNumberPlate(String numberPlate) {
        return QVehicle.vehicle.numberPlate.eq(numberPlate);
    }

    public static BooleanExpression freeVehicleByTime(LocalDateTime begin, LocalDateTime end) {
        return ReservationPredicate.notInTimespan(QVehicle.vehicle.reservations.any(), begin, end);
    }

    public static BooleanExpression freeVehicleByTimeAndSeatNr(LocalDateTime begin, LocalDateTime end, int seatNr) {
        return freeVehicleByTime(begin, end).and(hasSeatNr(seatNr));
    }

    public static BooleanExpression freeVehicleByTimeAndSeatNrAndBrand(LocalDateTime begin, LocalDateTime end, int seatNr, String brand) {
        return freeVehicleByTimeAndSeatNr(begin, end, seatNr).and(isBrand(brand));
    }

    public static BooleanExpression freeVehicleByTimeAndSeatNrAndBrandAndModel(LocalDateTime begin, LocalDateTime end, int seatNr, String brand, String model) {
        return freeVehicleByTimeAndSeatNrAndBrand(begin, end, seatNr, brand).and(isModel(model));
    }
}
