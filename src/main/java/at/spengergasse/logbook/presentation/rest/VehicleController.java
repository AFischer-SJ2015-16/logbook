package at.spengergasse.logbook.presentation.rest;

import at.spengergasse.logbook.config.FormatConfig;
import at.spengergasse.logbook.facade.VehicleFacade;
import at.spengergasse.logbook.service.view.VehicleView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("/api/Cars")
public class VehicleController {
    private final VehicleFacade vehicles;

    @GetMapping
    public List<VehicleView> getAllVehicles() {
        return vehicles.findAllVehicles();
    }

    @GetMapping("/{numberPlate}")
    public List<VehicleView> getVehiclesByNumberPlate(@PathVariable("numberPlate") String numberPlate) {
        return vehicles.findVehiclesByNumberPlate(numberPlate);
    }

    @GetMapping("/Available/{dateFrom:"+ FormatConfig.DATE_REGEX +"}/{dateTo:"+ FormatConfig.DATE_REGEX +"}")
    public List<VehicleView> getFreeVehiclesByTime(@PathVariable("dateFrom") LocalDate dateFrom, @PathVariable("dateTo") LocalDate dateTo) {
        return vehicles.findFreeVehicles(dateFrom.atStartOfDay(), dateTo.atTime(LocalTime.MAX));
    }
}
