package at.spengergasse.logbook.presentation.rest;

import at.spengergasse.logbook.config.FormatConfig;
import at.spengergasse.logbook.facade.EmployeeFacade;
import at.spengergasse.logbook.service.view.EmployeeEarningsView;
import at.spengergasse.logbook.service.view.EmployeeReservationsView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RestController
@RequestMapping("/api/Employee")
public class EmployeeController {
    private final EmployeeFacade employees;

    @GetMapping("/Reservations")
    public List<EmployeeReservationsView> getAllEmployeesWithReservations() {
        return employees.findAllEmployeesWithReservations();
    }

    @GetMapping("/{guid:"+ FormatConfig.GUID_REGEX+"}/Earnings")
    public EmployeeEarningsView getEmployeeEarnings(@PathVariable("guid") String guid) {
        return employees.findEmployeeWithEarnings(guid).orElse(null);
    }
}
