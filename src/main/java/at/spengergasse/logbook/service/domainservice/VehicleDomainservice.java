package at.spengergasse.logbook.service.domainservice;

import at.spengergasse.logbook.domain.Vehicle;
import at.spengergasse.logbook.persistence.predicate.VehiclePredicate;
import at.spengergasse.logbook.persistence.repository.VehicleRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static at.spengergasse.logbook.domain.QVehicle.vehicle;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class VehicleDomainservice {
    private final VehicleRepository vehicles;
    // Does this belong here? See comment below (at findBrands() method)
    private final JPAQueryFactory queries;

    public Iterable<Vehicle> findFreeVehiclesByTime(LocalDateTime begin, LocalDateTime end) {
        return vehicles.findAll(VehiclePredicate.freeVehicleByTime(begin, end));
    }

    public Iterable<Vehicle> findFreeVehiclesByCriteriaOld(LocalDateTime begin, LocalDateTime end, int seatNr, String brand, String model) {
        if(vehicles.count(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, seatNr, brand, model)) > 0) {
            return vehicles.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, seatNr, brand, model));
        } else if(vehicles.count(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrand(begin, end, seatNr, brand)) > 0) {
            return vehicles.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrand(begin, end, seatNr, brand));
        } else {
            return vehicles.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNr(begin, end, seatNr));
        }
    }

    public List<Vehicle> findVehicles() {
        return vehicles.findAll();
    }

    public Iterable<Vehicle> findVehiclesByNumberPlate(String numberPlate) {
        return vehicles.findAll(VehiclePredicate.hasNumberPlate(numberPlate));
    }

    /*
     * Using the JPAQueryFactory enables direct database access which actually is task of the persistence layer.
     * However the repositories are interfaces so no implementations are allowed normally.
     * When using default methods in the interfaces however, the problem is that the JPAQueryFactory cannot be accessed,
     * as it cannot be autowired.
     * A solution would be the following:
      * First another repository class (which is a class and no interface) is created which contains all methods which
      * use the JPAQueryFactory.
      * This has the drawback that now two different objects are necessary to access the same database tables.
      * A (bad) solution for this could be to create a utility class which exposes instances of both of these classes
      * through static fields or methods.
     */
    public List<String> findBrands() {
        return queries.select(vehicle.brand).from(vehicle).fetch();
    }

    // see comment above (findBrands() method)
    public List<Vehicle> findFreeVehiclesByCriteria(LocalDateTime begin, LocalDateTime end, int seatNr, String brand, String model) {
        BooleanExpression preComplete = VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, seatNr, brand, model);
        BooleanExpression preFallback = VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrand(begin, end, seatNr, brand);
        BooleanExpression countComplete = JPAExpressions.select(vehicle.count()).from(vehicle).where(preComplete).gt(0l);
        BooleanExpression countFallback = JPAExpressions.select(vehicle.count()).from(vehicle).where(preFallback).gt(0l);
        return queries.select(vehicle).from(vehicle).where(
                preComplete.and(countComplete)
                        .orAllOf(preFallback.and(countFallback))
                        .or(VehiclePredicate.freeVehicleByTimeAndSeatNr(begin, end, seatNr)))
                .fetch();
    }
}
