package at.spengergasse.logbook.service.domainservice;

import at.spengergasse.logbook.domain.Employee;
import at.spengergasse.logbook.domain.Reservation;
import at.spengergasse.logbook.domain.Trip;
import at.spengergasse.logbook.persistence.repository.EmployeeRepository;
import at.spengergasse.logbook.service.exception.DatabaseInconsistentException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class EmployeeDomainservice {
    private final EmployeeRepository employees;

    public List<Employee> findEmployees() {
        return employees.findAll();
    }

    public Optional<Employee> findById(String guid) {
        return Optional.of(employees.findOne(guid));
    }

    public Map<Trip, Reservation> mapTripsToReservations(Employee employee) {
        return employee.getTrips().stream().filter(trip -> trip.getEndDate() != null && trip.getKmEnd() != null)
                .collect(Collectors.toMap(Function.identity(), trip -> {
                    List<Reservation> reservations = employee.getReservations().stream().filter(reservation ->
                            reservation.getVehicle().getId().equals(trip.getVehicle().getId()) &&
                                    !reservation.getDateFrom().isAfter(trip.getStartDate()) &&
                                    !reservation.getDateTo().isBefore(trip.getStartDate()))
                            .collect(Collectors.toList());
                    if (reservations.size() == 0) {
                        log.error("No reservation found for the trip", trip);
                        throw new DatabaseInconsistentException();
                    } else if (reservations.size() > 1) {
                        log.error("Too many reservations (>1) found for the trip", trip);
                        throw new DatabaseInconsistentException();
                    }
                    return reservations.get(0);
                }));
    }
}
