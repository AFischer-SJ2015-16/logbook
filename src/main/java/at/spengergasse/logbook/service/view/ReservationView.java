package at.spengergasse.logbook.service.view;

import at.spengergasse.logbook.domain.Reservation;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.format.DateTimeFormatter;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
public class ReservationView {
    @Autowired
    private static DateTimeFormatter formatter;

    @JsonProperty("ReservationId")
    @NonNull
    private String id;
    @JsonProperty("NumberPlate")
    @NonNull
    private String numberPlate;
    @JsonProperty("StartDate")
    @NonNull
    private String dateFrom;
    @JsonProperty("EndDate")
    @NonNull
    private String dateTo;

    public static ReservationView of(Reservation reservation) {
        return new ReservationView(reservation.getId(), reservation.getVehicle().getNumberPlate(),
                reservation.getDateFrom().format(formatter), reservation.getDateTo().format(formatter));
    }
}