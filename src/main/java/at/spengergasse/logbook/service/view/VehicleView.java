package at.spengergasse.logbook.service.view;

import at.spengergasse.logbook.domain.Vehicle;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
public class VehicleView {
    @JsonProperty("ID")
    @NonNull
    private String id;
    @JsonProperty("Type")
    @NonNull
    private String type;
    @JsonProperty("Brand")
    @NonNull
    private String brand;
    @JsonProperty("Model")
    @NonNull
    private String model;
    @JsonProperty("NumberPlate")
    @NonNull
    private String numberPlate;
    @JsonProperty("SeatNr")
    private int seatNr;
    @JsonProperty("Km")
    @NonNull
    private BigDecimal km;
    @JsonProperty("BasicPrice")
    @NonNull
    private BigDecimal basicPrice;
    @JsonProperty("IncludedKm")
    private int includedKm;
    @JsonProperty("PricePer100Km")
    private BigDecimal pricePer100km;
    @JsonProperty("PenaltyPerDay")
    private BigDecimal penaltyPerDay;

    public static VehicleView of(Vehicle vehicle) {
        return new VehicleView(vehicle.getId(), vehicle.getType(), vehicle.getBrand(), vehicle.getModel(),
                vehicle.getNumberPlate(), vehicle.getSeatNr(), vehicle.getKm(), vehicle.getBasicPrice(),
                vehicle.getIncludedKm(), vehicle.getPricePer100km(), vehicle.getPenaltyPerDay());
    }
}