package at.spengergasse.logbook.service.view;

import at.spengergasse.logbook.domain.Employee;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
public class EmployeeReservationsView {
    @JsonProperty("ID")
    @NonNull
    private String id;
    @JsonProperty("Firstname")
    @NonNull
    private String firstname;
    @JsonProperty("Lastname")
    @NonNull
    private String lastname;
    @JsonProperty("Reservations")
    private List<ReservationView> reservations;

    public static EmployeeReservationsView of(Employee employee) {
        return new EmployeeReservationsView(employee.getId(), employee.getFirstname(), employee.getLastname(),
                employee.getReservations().stream().map(ReservationView::of).collect(Collectors.toList()));
    }
}
