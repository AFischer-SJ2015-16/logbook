package at.spengergasse.logbook.service.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
public class EmployeeEarningsView {
    @JsonProperty("ID")
    @NonNull
    private String id;
    @JsonProperty("Firstname")
    @NonNull
    private String firstname;
    @JsonProperty("Lastname")
    @NonNull
    private String lastname;
    @NonNull
    private BigDecimal earnings;

    @JsonProperty("Earnings")
    public String getEarnings() {
        return earnings.toPlainString();
    }
}
