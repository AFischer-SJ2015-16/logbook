package at.spengergasse.logbook.facade;

import at.spengergasse.logbook.domain.Vehicle;
import at.spengergasse.logbook.service.domainservice.VehicleDomainservice;
import at.spengergasse.logbook.service.view.VehicleView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class VehicleFacade {
    private final VehicleDomainservice vehicles;

    public List<VehicleView> findAllVehicles() {
        return fromList(vehicles.findVehicles());
    }

    public List<VehicleView> findVehiclesByNumberPlate(String numberPlate) {
        return fromIterable(vehicles.findVehiclesByNumberPlate(numberPlate));
    }

    public List<VehicleView> findFreeVehicles(LocalDateTime begin, LocalDateTime end) {
        return fromIterable(vehicles.findFreeVehiclesByTime(begin, end));
    }

    private List<VehicleView> fromIterable(Iterable<Vehicle> iterable) {
        return StreamSupport.stream(iterable.spliterator(), false).map(VehicleView::of).collect(Collectors.toList());
    }

    private List<VehicleView> fromList(List<Vehicle> list) {
        return list.stream().map(VehicleView::of).collect(Collectors.toList());
    }
}
