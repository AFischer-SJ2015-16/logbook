package at.spengergasse.logbook.facade;

import at.spengergasse.logbook.domain.Employee;
import at.spengergasse.logbook.domain.Reservation;
import at.spengergasse.logbook.domain.Trip;
import at.spengergasse.logbook.service.domainservice.EmployeeDomainservice;
import at.spengergasse.logbook.service.view.EmployeeEarningsView;
import at.spengergasse.logbook.service.view.EmployeeReservationsView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class EmployeeFacade {
    private final EmployeeDomainservice employees;

    public List<EmployeeReservationsView> findAllEmployeesWithReservations() {
        return fromListWithReservations(employees.findEmployees());
    }

    public Optional<EmployeeEarningsView> findEmployeeWithEarnings(String guid) {
        return getEarningsOfEmployee(employees.findById(guid));
    }

    private List<EmployeeReservationsView> fromListWithReservations(List<Employee> list) {
        return list.stream().map(EmployeeReservationsView::of).collect(Collectors.toList());
    }

    public Optional<EmployeeEarningsView> getEarningsOfEmployee(Optional<Employee> employee) {
        if (!employee.isPresent()) {
            Optional.empty();
        }
        Map<Trip, Reservation> map = employees.mapTripsToReservations(employee.get());
        BigDecimal earnings = map.keySet().stream().map(trip ->
                trip.getVehicle().getBasicPrice().add(
                        trip.getKmEnd().subtract(trip.getKmBegin()).subtract(BigDecimal.valueOf(trip.getVehicle().getIncludedKm())).max(BigDecimal.ZERO).setScale(-2, RoundingMode.CEILING)
                                .divide(BigDecimal.valueOf(100l)).multiply(trip.getVehicle().getPricePer100km()).setScale(2, RoundingMode.CEILING))
                        .add(
                                trip.getVehicle().getPenaltyPerDay()
                                        .multiply(
                                                BigDecimal.valueOf(
                                                        map.get(trip).getDateTo().toLocalDate().until(trip.getEndDate().toLocalDate(), ChronoUnit.DAYS)
                                                ).max(BigDecimal.ZERO)
                                        )
                        )
        ).collect(Collectors.reducing((o, o2) -> o.add(o2))).orElse(BigDecimal.ZERO);
        return Optional.of(new EmployeeEarningsView(employee.get().getId(), employee.get().getFirstname(), employee.get().getLastname(), earnings));
    }
}
