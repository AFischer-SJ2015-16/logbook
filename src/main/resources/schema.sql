-- Der Mitarbeiter, der Fahrzeuge vergeben kann.
drop table if exists Employee CASCADE;
drop table if exists Vehicle CASCADE;
drop table if exists Reservation CASCADE;
drop table if exists Trip CASCADE;

CREATE TABLE IF NOT EXISTS Employee (
  ID        CHAR(36)  PRIMARY KEY,
  Firstname VARCHAR(50) NOT NULL,
  Lastname  VARCHAR(50) NOT NULL,
  SVNR      VARCHAR(10) NOT NULL,
);

-- Beschreibt das Fahrzeug.
CREATE TABLE IF NOT EXISTS Vehicle (
  ID            CHAR(36)        PRIMARY KEY,
  Type          VARCHAR(25) DEFAULT 'PKW' NOT NULL,
  Brand         VARCHAR(32) DEFAULT 'no brand' NOT NULL,
  Model         VARCHAR(25) DEFAULT 'unknown' NOT NULL,
  NumberPlate   VARCHAR(50) UNIQUE NOT NULL,
  SeatNr        INT          NOT NULL,                    -- Anzahl der Plätze mit Fahrer.
  Km            DECIMAL(9,2) NOT NULL,                    -- Aktueller km Stand.
  BasicPrice    DECIMAL (8,4) NOT NULL,                   -- Grundpreis für die Vermietung.
  IncludedKm    INT           DEFAULT 0,                -- Km, die im Grundpreis enthalten sind.
  PricePer100Km DECIMAL (8,4),                            -- Preis pro angefangener 100km
  PenaltyPerDay DECIMAL (8,4)                             -- Wenn das Auto reserviert, aber verspätet
                                                            -- zurückgebracht wird, wird für jeden
                                                            -- angefangenen Tag dieser Preis verrechnet.
);

-- Über Internet können Kunden ein Auto reservieren. Dabei geben Sie an, von wann bis wann sie es
-- haben möchten. Im Geschäft wird diese Reservierung direkt vom Mitarbeiter an einem Terminal eingegeben.
CREATE TABLE IF NOT EXISTS Reservation (
  ID         CHAR(36) PRIMARY KEY,
  VehicleID  CHAR(36) NOT NULL,
  EmployeeID CHAR(36) NOT NULL,
  DateFrom   DATETIME(3)         NOT NULL,  -- Die Uhrzeit ist immer 0:00 UTC
  DateTo     DATETIME(3)         NOT NULL,  -- Die Uhrzeit ist immer 0:00 UTC
  FOREIGN KEY (VehicleID) REFERENCES Vehicle(ID),
  FOREIGN KEY (EmployeeID) REFERENCES Employee(ID),
);

-- Beim wirklichen Ausborgen wird ein Datensatz in Trip angelegt. Beim Zurückbringen wird dann in
-- den Feldern EndDate und KmEnd die aktuelle Information geschrieben.
CREATE TABLE IF NOT EXISTS Trip (
  ID         CHAR(36)       PRIMARY KEY,
  VehicleID  CHAR(36) NOT NULL,
  EmployeeID CHAR(36) NOT NULL,
  StartDate  DATETIME(3) NOT NULL,                            -- Datum der Übergabe des Fahrzeuges
  EndDate    DATETIME(3),                                     -- Datum der Rückgabe. NULL wenn das Auto unterwegs ist.
  KmBegin    DECIMAL(9,2) NOT NULL,                        -- KM Stand zu Beginn der Fahrt.
  KmEnd      DECIMAL(9,2),                                 -- KM Stand am Ende der Fahrt. NULL wenn das Auto unterwegs ist.


  FOREIGN KEY (VehicleID) REFERENCES Vehicle(ID),
  FOREIGN KEY (EmployeeID) REFERENCES Employee(ID)
);
