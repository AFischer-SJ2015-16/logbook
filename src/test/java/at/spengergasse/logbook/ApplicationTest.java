package at.spengergasse.logbook;

import at.spengergasse.logbook.domain.BaseDomain;
import at.spengergasse.logbook.domain.Vehicle;
import at.spengergasse.logbook.persistence.predicate.VehiclePredicate;
import at.spengergasse.logbook.persistence.repository.*;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class ApplicationTest {
    @Autowired
    private EmployeeRepository empRepo;
    @Autowired
    private ReservationRepository resRepo;
    @Autowired
    private VehicleRepository vehRepo;
    @Autowired
    private TripRepository triRepo;

    @Rule
    public ErrorCollector errors = new ErrorCollector();

    @Test
    public void testPersistence() {
        List<List<? extends BaseDomain>> results = new ArrayList<>();
        results.add(empRepo.findAll());
        results.add(resRepo.findAll());
        results.add(vehRepo.findAll());
        results.add(triRepo.findAll());
        results.forEach(res -> errors.checkThat(res.size(), Matchers.greaterThan(0)));
    }
}