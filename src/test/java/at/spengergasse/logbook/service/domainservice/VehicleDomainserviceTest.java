package at.spengergasse.logbook.service.domainservice;

import at.spengergasse.logbook.domain.Vehicle;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class VehicleDomainserviceTest {
    private static final LocalDateTime FREE_BEGIN = LocalDate.of(2000, 1, 1).atStartOfDay();
    private static final LocalDateTime FREE_END = LocalDate.of(2000, 1, 2).atStartOfDay();

    @Autowired
    private VehicleDomainservice vehDomSer;

    @Test
    public void testFindFreeVehiclesWithFullFallback() {
        List<Vehicle> list = findFreeVehicles(FREE_BEGIN, FREE_END, 7, "---notexisting---", "---notexisting---");
        Assert.assertEquals(6, list.size());
    }

    @Test
    public void testFindFreeVehiclesWithPartialFallback() {
        List<Vehicle> list = findFreeVehicles(FREE_BEGIN, FREE_END, 7, "BMW", "---notexisting---");
        Assert.assertEquals(2, list.size());
    }

    @Test
    public void testFindFreeVehiclesSpecific() {
        List<Vehicle> list  = findFreeVehicles(FREE_BEGIN, FREE_END, 7, "BMW", "S200");
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testFindFreeVehiclesEmpty() {
        List<Vehicle> list = findFreeVehicles(FREE_BEGIN, FREE_END, 0, "---notexisting---", "---notexisting---");
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testFindFreeVehiclesAlreadyReserved() {
        LocalDateTime begin = LocalDate.of(2000, 1, 1).atStartOfDay();
        LocalDateTime end = LocalDate.of(3000, 1, 1).atStartOfDay();
        List<Vehicle> list = findFreeVehicles(begin, end, 7, "BMW", "S200");
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testFindBrands() {
        List<String> result = vehDomSer.findBrands();
        Assert.assertThat(result.size(), Matchers.greaterThan(0));
    }

    @Test
    public void testFindFreeByCriteriaIsEqual() {
        List<Vehicle> res1 = findFreeVehicles(FREE_BEGIN, FREE_END, 7, "---notexisting---", "---notexisting---");
        List<Vehicle> res2 = vehDomSer.findFreeVehiclesByCriteria(FREE_BEGIN, FREE_END, 7, "---notexisting---", "---notexisting---");
        res1.forEach(vehicle1 -> {
            Assert.assertTrue(res2.stream().anyMatch(vehicle2 -> {
                return vehicle1.equals(vehicle2);
            }));
        });
    }

    private List<Vehicle> findFreeVehicles(LocalDateTime begin, LocalDateTime end, int seatNr, String brand, String model) {
        Iterable<Vehicle> result = vehDomSer.findFreeVehiclesByCriteriaOld(begin, end, seatNr, brand, model);
        List<Vehicle> list = new ArrayList<>();
        result.forEach(list::add);
        return list;
    }
}