package at.spengergasse.logbook.domain.converter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class LocalDateConverterTest {
    private LocalDateConverter test;

    @Before
    public void setup() {
        test = new LocalDateConverter();
    }

    @Test
    public void convertToDatabaseColumn() {
        LocalDateTime input = LocalDateTime.of(2016, 12, 31, 13, 59, 31);
        Timestamp output = test.convertToDatabaseColumn(input);
        Assert.assertEquals(input, output.toLocalDateTime());
    }

    @Test
    public void convertToEntityAttribute() {
        Timestamp input = Timestamp.valueOf(LocalDateTime.of(2016, 12, 31, 13, 59, 31));
        LocalDateTime output = test.convertToEntityAttribute(input);
        Assert.assertEquals(input, Timestamp.valueOf(output));
    }

}