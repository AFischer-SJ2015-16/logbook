package at.spengergasse.logbook.persistence.repository;

import at.spengergasse.logbook.domain.Vehicle;
import at.spengergasse.logbook.persistence.predicate.VehiclePredicate;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class VehicleRepositoryTest {
    @Autowired
    private VehicleRepository vehRepo;

    /*Database data:
    (ID,                                      Type,   Brand,   Model, NumberPlate, SeatNr, Km, BasicPrice, IncludedKm, PricePer100Km, PenaltyPerDay)
    ('608b8041-63f3-421d-b278-6bfe1132c89d',  'PKW',  'BMW',   'S200','BN-913-XY', 7,      0,  50,         1500,       40,            100);

    DATE '2014-02-04', TIMESTAMP '2014-02-13 05:00:00'
    DATE '2014-05-17', TIMESTAMP '2014-05-25 06:00:00'
    DATE '2014-09-15', TIMESTAMP '2014-09-23 06:00:00'
    DATE '2014-09-27', TIMESTAMP '2014-10-02 21:00:00'
    DATE '2014-11-15', TIMESTAMP '2014-11-25 20:00:00'
    DATE '2015-06-09', TIMESTAMP '2015-06-18 02:00:00'
    */

    @Test
    public void testVehiclesAvailableBefore() {
        LocalDateTime begin = LocalDate.of(2014, 1, 1).atStartOfDay();
        LocalDateTime end = LocalDate.of(2014, 2, 4).atStartOfDay();
        Iterable<Vehicle> result = vehRepo.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, 7, "BMW", "S200"));
        List<Vehicle> list = new ArrayList<>();
        result.forEach(list::add);
        Assert.assertEquals(1, list.size());
    }

    @Test
    public void testVehiclesAvailableBeginMatch() {
        LocalDateTime begin = LocalDate.of(2014, 2, 5).atStartOfDay();
        LocalDateTime end = LocalDate.of(2014, 2, 28).atStartOfDay();
        Iterable<Vehicle> result = vehRepo.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, 7, "BMW", "S200"));
        List<Vehicle> list = new ArrayList<>();
        result.forEach(list::add);
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testVehiclesAvailableEndMatch() {
        LocalDateTime begin = LocalDate.of(2014, 9, 1).atStartOfDay();
        LocalDateTime end = LocalDate.of(2014, 9, 22).atStartOfDay();
        Iterable<Vehicle> result = vehRepo.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, 7, "BMW", "S200"));
        List<Vehicle> list = new ArrayList<>();
        result.forEach(list::add);
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testVehiclesAvailableBothMatch() {
        LocalDateTime begin = LocalDate.of(2015, 6, 9).atStartOfDay();
        LocalDateTime end = LocalDate.of(2015, 6, 18).atStartOfDay();
        Iterable<Vehicle> result = vehRepo.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, 7, "BMW", "S200"));
        List<Vehicle> list = new ArrayList<>();
        result.forEach(list::add);
        Assert.assertEquals(0, list.size());
    }

    @Test
    public void testVehiclesAvailableAfter() {
        LocalDateTime begin = LocalDate.of(2015, 6, 19).atStartOfDay();
        LocalDateTime end = LocalDate.of(2016, 1, 31).atStartOfDay();
        Iterable<Vehicle> result = vehRepo.findAll(VehiclePredicate.freeVehicleByTimeAndSeatNrAndBrandAndModel(begin, end, 7, "BMW", "S200"));
        List<Vehicle> list = new ArrayList<>();
        result.forEach(list::add);
        Assert.assertEquals(1, list.size());
    }
}